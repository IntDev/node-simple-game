Simple Game

This project demonstrates an implementation of a simple browser game. Server code is written in Node.js. Client and server communicate via WebSockets via the socket.io module.

To start the project, do the following:
1. clone git repository
2. inside the repository execute npm install
3. start the server with node server.js
4. navigate to localhost:8080 in browser