function player(data) {
	var name = data.name;
	var speed = 5;
	var x = 100;
	var y = 50;
	var color = '#'+Math.floor(Math.random()*16777215).toString(16);

	function moveUp(){
		if (this.y > 0) this.y-=speed;
	}
	function moveDown(){
		if (this.y < 140) this.y+=speed;
	}
	function moveLeft(){
		if (this.x > 0) this.x-=speed;
	}
	function moveRight(){
		if (this.x < 250) this.x+=speed;
	}

	return {
		name: name,
		x: x,
		y: y,
		color: color,
		moveUp: moveUp,
		moveDown: moveDown,
		moveLeft: moveLeft,
		moveRight: moveRight
	}
}

exports.create = player;