var http = require('http');
var io = require('socket.io');
var fs = require('fs');
var url = require('url');
var router = require('./router').create(fs, url);
var player = require('./player');
var game = require('./game');

var server = http.createServer(function(req, res){
	router.route(req, res);
});

server.listen(8080);

var app = io.listen(server);
app.set('browser client minification', true);
app.set('browser client gzip', true);

game = game.create(app, player);

console.log('Server started.');