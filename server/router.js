function router(fs, url){
	function route(req, res) {
		var path = url.parse(req.url).pathname;
	
		var filePath;
		var contentType;

		console.log(path);
		if (/\.(css)$/.test(path)){ // css file
			filePath = __dirname + '/../client/content' + path;
			contentType = 'text/css';
		} else if (/\.(js)$/.test(path)) {
			filePath = __dirname + '/../client/scripts' + path;
			contentType = 'application/javascript';
		} else if (path == '/') {
			filePath = __dirname + '/../client/index.html';
			contentType = 'text/html';
		} else {
			res.writeHead(404);
			return res.end();
		}

		fs.readFile(filePath, function(err, data){
			if (err){
				console.log(err);
				res.writeHead(500);
				return res.end('Error loading resource.');
			}

			res.writeHead(200, { 'Content-Type': contentType });
			res.end(data);
		});
	}

	return {
		route: route
	};
}

exports.create = router;