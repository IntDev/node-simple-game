function game(app, factory){
	var players = [];

	function removePlayer(player){
		var index = players.indexOf(player);
		if (index > -1){
			players.splice(index, 1);
		}
	}

	function registerPlayerCommands(socket, player){
		// Disconnect
		socket.on('disconnect', function(){
			// Remove player from list
			removePlayer(player);
			draw();
			console.log('player ' + player.name + ' disconnected.');
		});

		// Move
		socket.on('move', function(data){
			console.log(data.direction);
			switch(data.direction){
				case 'left': player.moveLeft(); break;
				case 'up': player.moveUp(); break;
				case 'right': player.moveRight(); break;
				case 'down': player.moveDown(); break;
			}
			draw();
		});
	}

	function draw(){
		var data = [];
		for(var i = 0; i < players.length; i++){
			var player = players[i];
			data.push({
				name: player.name,
				x: player.x,
				y: player.y,
				color: player.color
			});
		}

		app.sockets.emit('draw', { players: data });
	}

	app.sockets.on('connection', function(socket){
		socket.on('connected', function(data){
			var player = factory.create(data);
			registerPlayerCommands(socket, player);
			players.push(player);
			draw();
			console.log('player ' + data.name + ' connected.');
		});
	});
}

exports.create = game;